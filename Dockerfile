FROM python:3.10.5-slim-buster

WORKDIR /app

COPY ./app.py .

COPY ./config.py .

COPY ./static ./static

COPY ./templates ./templates

COPY ./requirements.txt .

RUN pip install --upgrade pip setuptools wheel

RUN pip install -r requirements.txt

RUN rm requirements.txt

CMD ["flask", "run", "--host", "0.0.0.0", "--port", "5000"]
