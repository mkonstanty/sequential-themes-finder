"""Flask default configuration."""

FLASK_APP = "app"
SECRET_KEY = "efcdc4d982e458f1d116429c"
FLASK_ENV = "development"
DEBUG = True
TESTING = True
DATA_DIR = "./data"
STATIC_DIR = "./static"
