# Motif Search

Searching for sequential motifs of biological importance.

[[_TOC_]]

## Requirements

* [optional] git
* python >= 3.8

or

* docker

## Set up

```bash
git clone https://gitlab.com/mkonstanty/sequential-themes-finder.git
```

or just use download button and unpack all the files.

```bash
cd sequential-themes-finder
```

### Run application in docker container

```bash
docker build -t motif_search .
docker run -p 80:5000 motif_search
```

### Run application in local environment

Install all python dependencies (there is high recommendation to use python virtual environment of user's choice)

```
pip install -r requirements.txt

```
and run the launcher:

```bash
./run.sh
```

### Usage as script

Open url in web browser http://localhost:5000 (or http://localhost if running in a docker).

## Sequence Motif

Application requires MOTIF in PROSITE format as major input - one-letter codes concatenated with dash symbol. Optionally for simple patterns user may try to omit dash symbol and input Motif without it, ie. W-x-x-A-K -> WxxAKK. But this is not fully supported.

* A, R, D, N, C, E, Q, G, H, I, L, K, M, F, P, S, T, W, Y, V - amino acid symbol
* x - any amino acid symbol
* {DE} - any amino acid except D or E
* [DE] - either D or E amino acid symbol
* A(3) - A symbol appears 3 times consecutively
* A(2,3) - A appears 2 to 3 times consecutively

### Databases

Another required input is database. At the beginning user can choose one of two online searches, but once downloaded db locally application allows user to make local searches. Also there is possibility to just upload FASTA file to me used as custom database. Multiple file types are accepted: plain text, gzip, zip and gz2.

### Pattern examples
- [EQ]-x-L-Y-[DEQST]-x(3,12)-[LIV]-[ST]-Y-x-R-[ST]-[DEQS]
- W-x-x-A-K
- P-x(2)-G-E-S-G(2)-[AS]

## Author

Mariusz Konstanty

Engineering work under the supervision of Dr. Karolina Anna Mikulska-Rumińska

Faculty of Physics, Astronomy and Applied Computer Science at the Nicolaus Copernicus University in Toruń

[GitLab: Sequential Themes Finder](https://gitlab.com/mkonstanty/sequential-themes-finder)
