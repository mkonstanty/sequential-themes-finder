"""Flask default configuration."""

FLASK_APP = "app"
SECRET_KEY = "efcdc4d982e458f1d116429c"
FLASK_ENV = "production"
DEBUG = False
TESTING = False
DATA_DIR = "./data"
STATIC_DIR = "./static"
UPLOAD_DIR = DATA_DIR + "/local"
